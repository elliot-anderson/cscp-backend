import { Knex } from "knex";
import * as dotenv from "dotenv";

dotenv.config();

const knexfile: {
	development: Knex.Config;
	production: Knex.Config;
} = {
	development: {
		client: "postgresql",
		connection: {
			host: "localhost",
			database: process.env.DATABASE_NAME,
			user: process.env.DATABASE_USER,
			password: process.env.DATABASE_PASSWORD,
		} as Knex.PgConnectionConfig,
		migrations: {
			extension: "ts",
			directory: `${__dirname}/src/migrations`,
		},
		seeds: {
			directory: `${__dirname}/src/seeds`,
		},
	},
	production: {
		client: "postgresql",
		connection: {
			database: process.env.DATABASE_NAME,
			user: process.env.DATABASE_USER,
			password: process.env.DATABASE_PASSWORD,
		} as Knex.PgConnectionConfig,
		migrations: {
			extension: "ts",
			directory: `${__dirname}/src/migrations`,
		},
		seeds: {
			directory: `${__dirname}/src/seeds`,
		},
	},
};

export default knexfile;