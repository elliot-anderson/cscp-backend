import express from "express";
import users from "./src/routes/users";
import student_profile from "./src/routes/student_profiles";
import vacancies from "./src/routes/vacancies";
import cors from "cors";
import logger from "morgan";
import cookieParser from "cookie-parser";
import path from "path";

const app: express.Application = express();
app.use(function(_, res:express.Response, next:express.NextFunction) {
	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
app.use("/users", users);
app.use("/users", student_profile);
app.use("/company", vacancies);
const port = process.env.PORT || "8080";
app.listen(port as string, () =>
	console.log("API Service started at %s ...", port)
);
