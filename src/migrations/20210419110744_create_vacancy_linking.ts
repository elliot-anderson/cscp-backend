import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("vacancy_linking", (table) => {
		table.increments().primary();
		table.integer("vacancy_id").references("vacancies.id").notNullable();
		table.integer("student_id").references("users.id");
		table.enu("status", [
			"applied",
			"in_review",
			"interview",
			"rejected",
			"hired",
		]);
		table.string("comp_message");
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("vacancy_linking");
}
