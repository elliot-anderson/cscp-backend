import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("companies", (table) => {
		table.increments().primary();
		table.string("name").unique();
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("companies");
}
