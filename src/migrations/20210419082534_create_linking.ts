import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("linking", (table) => {
		table.integer("user_id").references("users.id").notNullable().primary();
		table.integer("uni_id").references("universities.id");
		table.integer("comp_id").references("companies.id");
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("linking");
}
