import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	const universities = [
		"Academy of Public Administration",
		"Azerbaijan Architecture and Construction University",
		"Azerbaijan Medical University",
		"Azerbaijan State Agricultural University",
		"Azerbaijan State Economic University",
		"Azerbaijan State Marine Academy",
		"Azerbaijan State Oil and Industry University",
		"Azerbaijan State University of Culture and Arts",
		"Academy of State Customs Committee of the Republic of Azerbaijan",
		"Azerbaijan University of Architecture and Construction",
		"Azerbaijan Tourism and Management University",
		"National Conservatory of Azerbaijan",
		"Azerbaijan State Pedagogical University",
		"Azerbaijan Technical University",
		"Azerbaijan University of Languages",
		"Baku Engineering University",
		"Baku Academy of Music",
		"Baku Higher Oil School",
		"National Aviation Academy (Azerbaijan)",
		"Baku Slavic University",
		"Baku State University",
		"Theology Institute of Azerbaijan",
		"Ganja State University",
		"Lankaran State University",
		"Mingachevir State University",
		"Nakhchivan State University",
		"Sumqayit State University",
		"Khazar University",
		"Baku Business University",
		"Azerbaijan University",
		"Baku Eurasian University",
		"Western Caspian University",
		"Azerbaijan University of Cooperation",
		"Nakchivan Private University",
		"Odlar Yurdu University",
		"Western University",
		"Azerbaijan university, college and institute directory",
		"Universities in Azerbaijan",
		"Azerbaijan Diplomatic Academy",
		"Azerbaijan State Oil Academy",
		"Mingachevir Polytechnic Institute",
		"Qafqaz University",
	];
	await knex.schema.createTable("universities", (table) => {
		table.increments().primary();
		table.enu("name", universities).unique();
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("universities");
}
