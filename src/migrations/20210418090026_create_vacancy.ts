import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("vacancies", (table) => {
		table.increments().primary();
		table.integer("author").references("users.id");
		table.integer("company").references("companies.id");
		table.string("title").notNullable();
		table.string("body").notNullable();
		table.date("start").notNullable();
		table.date("end");
		table.date("updated");
		table.date("archived");
		table.integer("num_of_positions");
		table.integer("num_of_hired").defaultTo(0);
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("vacancies");
}
