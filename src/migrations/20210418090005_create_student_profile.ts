import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("student_profiles", (table) => {
		table.integer("student_id").references("users.id").primary();
		table.string("phone_number").notNullable();

		table
			.enu("employment_status", ["employed", "unemployed"])
			.notNullable();
		table
			.enu("job_opportunities", [
				"actively_looking",
				"open_but_not_active",
				"not_interested",
			])
			.notNullable();
		table.string("job_title").notNullable();
		table.string("objectives").notNullable();
		table.specificType("institute_names_array", "text ARRAY").notNullable();
		table
			.specificType("institute_degree_names_array", "text ARRAY")
			.notNullable();
		table
			.specificType("institute_locations_array", "text ARRAY")
			.notNullable();
		table
			.specificType("field_of_studies_array", "text ARRAY")
			.notNullable();
		table.specificType("study_intervals_array", "text ARRAY").notNullable();
		table.specificType("gpa_score", "text ARRAY");
		table.specificType("company_names_array", "text ARRAY");
		table.specificType("company_locations_array", "text ARRAY");
		table.specificType("job_titles_array", "text ARRAY");
		table.specificType("job_descriptions_array", "text ARRAY");
		table.specificType("work_intervals_array", "text ARRAY");

		table.specificType("project_titles_array", "text ARRAY");
		table.specificType("project_descriptions_array", "text ARRAY");

		table.specificType("skills_array", "text ARRAY");
		table.specificType("skill_levels_array", "integer ARRAY");

		table.string("area_of_interests");
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("student_profiles");
}
