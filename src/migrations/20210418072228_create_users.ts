import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
	await knex.schema.createTable("users", (table) => {
		table.increments().primary();
		table
			.enu("role", ["student", "university", "company"], {
				useNative: true,
				enumName: "roles",
			})
			.notNullable();
		table.string("fname").notNullable();
		table.string("mname");
		table.string("lname").notNullable();
		table.string("email").notNullable().unique();
		table.string("password_hash").notNullable();
	});
}

export async function down(knex: Knex): Promise<void> {
	await knex.schema.dropTable("users");
}
