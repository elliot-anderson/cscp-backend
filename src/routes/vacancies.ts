import express, { Router } from "express";
import {
	getVacancies,
	createVacancy,
	updateVacancy,
	deleteVacancy,
} from "../db/queries";
import { authChecker, compChecker } from "./authentication_mod";
const router: Router = Router();

router.get("/vacancies", authChecker, (_, res: express.Response) => {
	getVacancies()
		.then((data: any) => res.json(data))
		.catch((err: any) => res.json(err.name));
});
router.post(
	"/vacancies",
	authChecker,
	compChecker,
	(req: express.Request, res: express.Response) => {
		if (req.body.authres.user_id === req.body.author) {
			createVacancy(
				req.body.author,
				req.body.company,
				req.body.title,
				req.body.body,
				req.body.start,
				req.body.end,
				req.body.updated,
				req.body.archived,
				req.body.num_of_positions,
				req.body.num_of_hired
			)
				.then((data: any) => res.json(data))
				.catch((err: any) => res.json(err.name));
		} else {
			res.sendStatus(403);
		}
	}
);
router.put(
	"/vacancies",
	authChecker,
	compChecker,
	(req: express.Request, res: express.Response) => {
		if (req.body.authres.user_id === req.body.author) {
			updateVacancy(
				req.body.id,
				req.body.author,
				req.body.company,
				req.body.title,
				req.body.body,
				req.body.start,
				req.body.end,
				req.body.updated,
				req.body.archived,
				req.body.num_of_positions,
				req.body.num_of_hired
			)
				.then((data: any) => res.json(data))
				.catch((err: any) => res.json(err.name));
		} else res.sendStatus(403);
	}
);
router.delete(
	"/vacancies",
	authChecker,
	compChecker,
	(req: express.Request, res: express.Response) => {
		deleteVacancy(req.body.id, req.body.authres.user_id)
			.then((data: any) => res.json(data))
			.catch((_: any) => res.sendStatus(403));
	}
);

export default router;
