import express, { Router } from "express";
import {
	getStudentProfiles,
	getStudentProfilesUni,
	getStudentProfile,
	createStudentProfile,
	updateStudentProfile,
} from "../db/queries";
import { authChecker, compChecker, uniChecker } from "./authentication_mod";
const router: Router = Router();

router.get(
	"/students",
	authChecker,
	compChecker,
	(_, res: express.Response) => {
		getStudentProfiles()
			.then((data: any) => res.json(data))
			.catch((err: any) => res.json(err.name));
	}
);
router.get(
	"/mystudents",
	authChecker,
	uniChecker,
	(req: express.Request, res: express.Response) => {
		console.log(JSON.stringify(req.body));
		getStudentProfilesUni(req.body.authres.user_id)
			.then((data: any) => res.json(data))
			.catch((err: any) => res.json(err.name));
	}
);
router.get(
	"/myprofile",
	authChecker,
	(req: express.Request, res: express.Response) => {
		getStudentProfile(req.body.authres.user_id)
			.then((data: any) => res.json(data))
			.catch((err: any) => res.json(err.name));
	}
);
router.post(
	"/myprofile",
	authChecker,
	(req: express.Request, res: express.Response) => {
		createStudentProfile(
			req.body.student_id as number,
			req.body.phone_number,
			req.body.employment_status,
			req.body.job_opportunities,
			req.body.job_title,
			req.body.objectives,
			req.body.institute_names_array,
			req.body.institute_degree_names_array,
			req.body.institute_locations_array,
			req.body.field_of_studies_array,
			req.body.study_intervals_array,
			req.body.gpa_score,
			req.body.company_names_array,
			req.body.company_locations_array,
			req.body.job_titles_array,
			req.body.job_descriptions_array,
			req.body.work_intervals_array,
			req.body.project_titles_array,
			req.body.project_descriptions_array,
			req.body.skills_array,
			req.body.skill_levels_array,
			req.body.area_of_interests
		)
			.then((data: any) => res.json(data))
			.catch((err: any) => res.json(err.name));
	}
);
router.put(
	"/myprofile",
	authChecker,
	(req: express.Request, res: express.Response) => {
		updateStudentProfile(
			req.body.student_id as number,
			req.body.phone_number,
			req.body.employment_status,
			req.body.job_opportunities,
			req.body.job_title,
			req.body.objectives,
			req.body.institute_names_array,
			req.body.institute_degree_names_array,
			req.body.institute_locations_array,
			req.body.field_of_studies_array,
			req.body.study_intervals_array,
			req.body.gpa_score,
			req.body.company_names_array,
			req.body.company_locations_array,
			req.body.job_titles_array,
			req.body.job_descriptions_array,
			req.body.work_intervals_array,
			req.body.project_titles_array,
			req.body.project_descriptions_array,
			req.body.skills_array,
			req.body.skill_levels_array,
			req.body.area_of_interests
		)
			.then((data: any) => res.json(data))
			.catch((err: any) => res.json(err));
	}
);
export default router;
