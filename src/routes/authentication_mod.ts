import express from "express";
import { decodeToken } from "../middleware/auth";

export const authChecker = (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
): void => {
	const authHeader: any = req.headers.authorization;

	if (authHeader) {
		const token: string = authHeader.split(" ")[1];

		const result: any = decodeToken(token);
		if (result === "Invalid Token") {
			res.sendStatus(401);
		} else {
			req.body.authres = result;
			next();
		}
	} else {
		res.sendStatus(401);
	}
};
export const compChecker = (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
): void => {
	if (req.body.authres.role === "company") {
		next();
	} else {
		res.sendStatus(403);
	}
};

export const uniChecker = (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
): void => {
	if (req.body.authres.role === "university") {
		next();
	} else {
		res.sendStatus(403);
	}
};
