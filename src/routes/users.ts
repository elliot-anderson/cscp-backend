import { Request, Response, Router } from "express";
import { login, createUser } from "../db/queries";
import { getToken } from "../middleware/auth";

const router: Router = Router();

router.post("/signup", (req: Request, res: Response) => {
	if(!req.body.role||!req.body.fname||!req.body.lname||!req.body.email||!req.body.password)
		res.status(401).json({ type:false, error: "All the required fields must be filled!"});
	else
	createUser(
		req.body.role,
		req.body.fname,
		req.body.mname,
		req.body.lname,
		req.body.email,
		req.body.password
	)
		.then((user: any) => {
			res.json(user);
		})
		.catch((error) => res.status(400).json({ error: error.message }));
});
router.post("/login", (req: Request, res: Response) => {
	if (req.body.email == "" || req.body.password == "") {
		res.status(401).json({ type:false, error: "Both email and password are required!" });
	} else {
		login(req.body.email, req.body.password)
			.then((user: any) => {
				const myToken: string = getToken(
					user.id,
					user.role,
					user.fname,
					user.mname,
					user.lname,
					user.email
				);
				const myHeaders: any = {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${myToken}`,
				};
				const {password_hash, ...userSafe} = user;
				res.set(myHeaders);
				res.json({type:true, user:userSafe, token:myToken});
			})
			.catch((err: any) => {
				res.status(401).send({type:false,  error: err.message });
			});
	}
});
export default router;
