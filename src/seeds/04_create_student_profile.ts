import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE student_profiles RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("student_profiles").insert([
				{
					student_id: 1,
					phone_number: "(+994)55 394 59 17",
					employment_status: "employed",
					job_opportunities: "actively_looking",
					job_title: "Full-stack Software Developer",
					objectives:
						"I am a full-stack software developer with more than 1 year of experience with Node.js, Express.js, Knex.js, React technologies. I'm motivated, result-focused and seeking a successful team-oriented company with opportunity to grow.",
					institute_names_array: ["Baku Higher Oil School"],
					institute_degree_names_array: ["Bachelor degree"],
					institute_locations_array: ["Baku"],
					field_of_studies_array: ["Process Automation Engineering"],
					study_intervals_array: ["2016-2021"],
					gpa_score: ["84/100"],
					company_names_array: ["Kernel LLC"],
					company_locations_array: ["Baku"],
					job_titles_array: ["Full-stack software developer"],
					job_descriptions_array: [
						"I with my core team members develop web and mobile applications using Node.js, React software tools",
					],
					work_intervals_array: ["2020-continuing"],
					project_titles_array: ["Web project 1"],
					project_descriptions_array: [
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					],
					skills_array: ["TypeScript", "Node.js", "React.js"],
					skill_levels_array: [2, 3, 3],
					area_of_interests:
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				},
				{
					student_id: 2,
					phone_number: "(+994)55 555 55 55",
					employment_status: "employed",
					job_opportunities: "actively_looking",
					job_title: "Database Administrator",
					objectives:
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					institute_names_array: ["Baku State University"],
					institute_degree_names_array: ["Bachelor degree"],
					institute_locations_array: ["Baku"],
					field_of_studies_array: ["Computer Science"],
					study_intervals_array: ["2014-2018"],
					gpa_score: ["91/100"],
					company_names_array: ["IDRAK Technology Transfer"],
					company_locations_array: ["Baku"],
					job_titles_array: ["DBA"],
					job_descriptions_array: [
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					],
					work_intervals_array: ["2018-continuing"],
					project_titles_array: ["DBA project 1"],
					project_descriptions_array: [
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					],
					skills_array: ["Linux", "Oracle Database"],
					skill_levels_array: [3, 4],
					area_of_interests:
						"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				},
			]);
		});
}
