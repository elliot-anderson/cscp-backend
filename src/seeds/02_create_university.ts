import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE universities RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("universities").insert([
				{ name: "Baku Higher Oil School" },
				{ name: "Baku State University" },
			]);
		});
}
