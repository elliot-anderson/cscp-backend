import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE companies RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("companies").insert([
				{ name: "Kernel LLC" },
				{ name: "IDRAK Technology Transfer" },
			]);
		});
}
