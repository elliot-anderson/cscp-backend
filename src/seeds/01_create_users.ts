import { Knex } from "knex";
import argon2 from "argon2";

export async function seed(knex: Knex): Promise<void> {
	const roles = ["student", "university", "company"];
	await knex
		.raw("TRUNCATE TABLE users RESTART IDENTITY CASCADE")
		.then(async () => {
			let users: any[] = [
				{
					role: roles[0],
					fname: "Jamal",
					lname: "Muradov",
					email: "jamal.muradov.b0605@bhos.edu.az",
					password: "Salam183",
				},
				{
					role: roles[0],
					fname: "Jabbar",
					lname: "Muradov",
					email: "jabbar.muradov@sabah.edu.az",
					password: "Salam183",
				},
				{
					role: roles[1],
					fname: "Kamala",
					lname: "Pashayeva",
					email: "kamala.pashayeva@bhos.edu.az",
					password: "Salam183",
				},
				{
					role: roles[1],
					fname: "Sabir",
					lname: "Pashayev",
					email: "sabir.pashayev@sabah.edu.az",
					password: "Salam183",
				},
				{
					role: roles[2],
					fname: "Kanan",
					lname: "Shirinov",
					email: "kanan.shirinov@kernelabc.com",
					password: "Salam183",
				},
				{
					role: roles[2],
					fname: "Kamil",
					lname: "Qadirli",
					email: "kamil.qadirli@idrak.com",
					password: "Salam183",
				},
			];
			for (let user of users) {
				user.password_hash = await argon2.hash(user.password, {
					type: 2,
				});
			}
			await knex("users").insert(
				users.map(({ password, ...rest }) => rest)
			);
		});
	await knex.raw(
		"create view users_safe as (select id, role, fname,\
		 mname, lname, email from users);"
	);
}
