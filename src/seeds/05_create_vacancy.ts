import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE vacancies RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("vacancies").insert([
				{
					author: 5,
					company: 1,
					title: "Software developer",
					body: "We are looking for part time software developers",
					start: "12.05.2021",
					num_of_positions: 25,
				},
				{
					author: 6,
					company: 2,
					title: "DBA",
					body: "We are looking for part time DBAs",
					start: "12.05.2021",
					num_of_positions: 25,
				},
			]);
		});
}
