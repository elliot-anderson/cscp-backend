import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE linking RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("linking").insert([
				{ user_id: 1, uni_id: 1, comp_id: 1 },
				{ user_id: 2, uni_id: 2, comp_id: 2 },
				{ user_id: 3, uni_id: 1 },
				{ user_id: 4, uni_id: 2 },
				{ user_id: 5, comp_id: 1 },
				{ user_id: 6, comp_id: 2 },
			]);
		});
}
