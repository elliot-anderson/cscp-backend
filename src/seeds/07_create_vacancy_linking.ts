import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
	await knex
		.raw("TRUNCATE TABLE linking RESTART IDENTITY CASCADE")
		.then(async () => {
			await knex("vacancy_linking").insert([
				{
					vacancy_id: 1,
					student_id: 1,
					status: "hired",
					comp_message: "You are hired!",
				},
				{
					vacancy_id: 2,
					student_id: 2,
					status: "hired",
					comp_message: "You are hired!",
				},
			]);
		});
}
