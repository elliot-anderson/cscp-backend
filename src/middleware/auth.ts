import jwt from "jsonwebtoken";

const privateKey: string =
	process.env.JWT_SECRET ||
	"S@cred@ppl1c4t10nW4sH@ck3dByJ0k3r!1mp0rt4nt$+/r3g3xW3sH3r3";

export const getToken = (
	userID: number,
	role: string,
	fname: string,
	mname = "",
	lname: string,
	email: string
) => {
	const payload = {
		user_id: userID,
		role: role,
		fname: fname,
		mname: mname,
		lname: lname,
		email: email,
	};
	return jwt.sign(payload, privateKey, {
		algorithm: "HS256",
		noTimestamp: true,
	});
};
export const decodeToken = (token: string): any => {
	return jwt.verify(token, privateKey, (err, user) => {
		if (err) return "Invalid Token";
		else return user;
	});
};
