import knex, {Knex} from "knex";
import knexfile from "../../knexfile";

const environment: string = process.env.NODE_ENV || "development";
let config: Knex.Config;
if (environment === "development")
    config = knexfile.development;
else if (environment === "production")
    config = knexfile.production;
else
    config = knexfile.development;

export default knex(config);
