import knex from "./knex";
import argon2 from "argon2";

export const login = async (email: string, password: string) => {
	let getUser = await knex("users").where("email", email);
	let user = getUser[0];
	if (await argon2.verify(user.password_hash, password)) return user;
	else throw new Error("Invalid email or password");
};
export const createUser = async (
	role: string,
	fname: string,
	mname = null,
	lname: string,
	email: string,
	password: string
) => {
	try {
		let password_hash = await argon2.hash(password, {
			type: 2,
		});
		return await knex("users")
			.returning(["role", "fname", "lname", "email"])
			.insert({
				role: role,
				fname: fname,
				mname: mname,
				lname: lname,
				email: email,
				password_hash: password_hash,
			});
	} catch (err) {
		throw new Error("Registration failed: Invalid data!");
	}
};

export const getStudentProfiles = async () => {
	return await knex
		.select("*")
		.from("student_profiles")
		.innerJoin(
			"users_safe",
			"users_safe.id",
			"=",
			"student_profiles.student_id"
		);
};
export const getStudentProfilesUni = async (user_id: number) => {
	return await knex
		.select("*")
		.from("users_safe")
		.innerJoin("linking", "users_safe.id", "=", "linking.user_id")
		.innerJoin(
			"student_profiles",
			"users_safe.id",
			"=",
			"student_profiles.student_id"
		)
		.where({
			"linking.uni_id": knex
				.select("linking.uni_id")
				.from("linking")
				.innerJoin(
					"users_safe",
					"users_safe.id",
					"=",
					"linking.user_id"
				)
				.where({
					"linking.user_id": user_id,
					"users_safe.role": "university",
				}),
		});
};

export const getStudentProfile = async (student_id: number) => {
	return await knex
		.select("*")
		.from("users_safe")
		.innerJoin(
			"student_profiles",
			"users_safe.id",
			"=",
			"student_profiles.student_id"
		)
		.where({ "student_profiles.student_id": student_id });
};

export const createStudentProfile = async (
	student_id: number,
	phone_number: string,
	employment_status: string,
	job_opportunities: string,
	job_title: string,
	objectives: string,
	institute_names_array: string[],
	institute_degree_names_array: string[],
	institute_locations_array: string[],
	field_of_studies_array: string[],
	study_intervals_array: string[],
	gpa_score: string[],
	company_names_array: string[],
	company_locations_array: string[],
	job_titles_array: string[],
	job_descriptions_array: string[],
	work_intervals_array: string[],
	project_titles_array: string[],
	project_descriptions_array: string[],
	skills_array: string[],
	skill_levels_array: number[],
	area_of_interests: string
) => {
	await knex("student_profiles").insert([
		{
			student_id: student_id,
			phone_number: phone_number,
			employment_status: employment_status,
			job_opportunities: job_opportunities,
			job_title: job_title,
			objectives: objectives,
			institute_names_array: institute_names_array,
			institute_degree_names_array: institute_degree_names_array,
			institute_locations_array: institute_locations_array,
			field_of_studies_array: field_of_studies_array,
			study_intervals_array: study_intervals_array,
			gpa_score: gpa_score,
			company_names_array: company_names_array,
			company_locations_array: company_locations_array,
			job_titles_array: job_titles_array,
			job_descriptions_array: job_descriptions_array,
			work_intervals_array: work_intervals_array,
			project_titles_array: project_titles_array,
			project_descriptions_array: project_descriptions_array,
			skills_array: skills_array,
			skill_levels_array: skill_levels_array,
			area_of_interests: area_of_interests,
		},
	]);
};
export const updateStudentProfile = async (
	student_id: number,
	phone_number: string,
	employment_status: string,
	job_opportunities: string,
	job_title: string,
	objectives: string,
	institute_names_array: string[],
	institute_degree_names_array: string[],
	institute_locations_array: string[],
	field_of_studies_array: string[],
	study_intervals_array: string[],
	gpa_score: string[],
	company_names_array: string[],
	company_locations_array: string[],
	job_titles_array: string[],
	job_descriptions_array: string[],
	work_intervals_array: string[],
	project_titles_array: string[],
	project_descriptions_array: string[],
	skills_array: string[],
	skill_levels_array: number[],
	area_of_interests: string
) => {
	await knex("student_profiles")
		.where("student_profiles.student_id", student_id)
		.update({
			student_id: student_id,
			phone_number: phone_number,
			employment_status: employment_status,
			job_opportunities: job_opportunities,
			job_title: job_title,
			objectives: objectives,
			institute_names_array: institute_names_array,
			institute_degree_names_array: institute_degree_names_array,
			institute_locations_array: institute_locations_array,
			field_of_studies_array: field_of_studies_array,
			study_intervals_array: study_intervals_array,
			gpa_score: gpa_score,
			company_names_array: company_names_array,
			company_locations_array: company_locations_array,
			job_titles_array: job_titles_array,
			job_descriptions_array: job_descriptions_array,
			work_intervals_array: work_intervals_array,
			project_titles_array: project_titles_array,
			project_descriptions_array: project_descriptions_array,
			skills_array: skills_array,
			skill_levels_array: skill_levels_array,
			area_of_interests: area_of_interests,
		});
};

export const createVacancy = async (
	author: number,
	company: number,
	title: string,
	body: string,
	start: Date,
	end: Date | any,
	updated: Date | any,
	archived: Date | any,
	num_of_positions: number,
	num_of_hired: number | any
) => {
	await knex("vacancies").insert([
		{
			author: author,
			company: company,
			title: title,
			body: body,
			start: start,
			end: end,
			updated: updated,
			archived: archived,
			num_of_positions: num_of_positions,
			num_of_hired: num_of_hired,
		},
	]);
};
export const updateVacancy = async (
	vacancy_id: number,
	author: number,
	company: number,
	title: string,
	body: string,
	start: Date,
	end: Date | any,
	updated: Date | any,
	archived: Date | any,
	num_of_positions: number,
	num_of_hired: number | any
) => {
	await knex("vacancies").where("vacancies.id", vacancy_id).update({
		author: author,
		company: company,
		title: title,
		body: body,
		start: start,
		end: end,
		updated: updated,
		archived: archived,
		num_of_positions: num_of_positions,
		num_of_hired: num_of_hired,
	});
};
export const deleteVacancy = async (vacancy_id: number, author_id: number) => {
	const content = await knex.select("*").from("vacancies").where({
		"vacancies.id": vacancy_id,
		"vacancies.author": author_id,
	});
	const econtent = await knex.select("*").from("vacancies").where({
		"vacancies.id": vacancy_id,
	});
	if (content.length > 0)
		await knex("vacancies")
			.where({
				"vacancies.id": vacancy_id,
				"vacancies.author": author_id,
			})
			.del();
	else if (econtent.length > 0) throw new Error("Forbidden");
};
export const getVacancies = async () => {
	return await knex
		.select([
			"vacancies.*",
			"users_safe.role",
			"users_safe.fname",
			"users_safe.mname",
			"users_safe.lname",
			"users_safe.email",
			"companies.name",
		])
		.from("vacancies")
		.innerJoin("users_safe", "users_safe.id", "=", "vacancies.author")
		.innerJoin("companies", "companies.id", "=", "vacancies.company");
};
